variables:
  # Tell docker CLI how to talk to Docker daemon; see
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
  GIT_SUBMODULE_STRATEGY: recursive
  # Foundation specific pipeline toggle
  FOUNDATION_CHECK_DOCS: "true"            # Check documentation consistency
  FOUNDATION_CHECK_FORMAT: "true"          # Check code format compliance
  FOUNDATION_CHECK_LINT: "true"            # Check code lint compliance
  FOUNDATION_CHECK_IMPORTS: "true"         # Check import sorting compliance
  FOUNDATION_CHECK_TYPES: "true"           # Check type hint annotations 
  FOUNDATION_PUBLISH_PACKAGE: "true"       # Publish the python package
  FOUNDATION_PUBLISH_IMAGES: "true"        # Publish the docker image
  FOUNDATION_PUBLISH_PAGES: "true"         # Publish documentation & coverage pages

stages:
  - test              # test, linting and security
  - build             # build & publish python package or images to their respective gitlab's registry
  - release           # release dev to main

.foundation:
  # default setup steps for using the foundation bootstrap
  image: registry.gitlab.com/construction-zone/bedrock/foundation:venv
  before_script:
    # move the repository into scope of the foundation artifacts
    - |
      echo "Moving the cloned repository in scope of the foundation artifacts."
      export WORKING_DIR=/foundation/packages/python/
      cp -r ../${CI_PROJECT_NAME} ${WORKING_DIR}
      cd ${WORKING_DIR}/${CI_PROJECT_NAME}
    # grab the current version and add the postfix dev tag if needed
    - |
      export VERSION=$(awk 'NR==2' .bumpversion)
      if [[ "${CI_COMMIT_BRANCH}" == "develop" ]]; then
        echo "Creating a develop version based on CI_PIPELINE_CREATED_AT"
        export DEV_TAG=.dev$(date +'%s' --date ${CI_PIPELINE_CREATED_AT})
        export VERSION=${VERSION}${DEV_TAG}
      fi
      echo VERSION: $(awk 'NR==2' .bumpversion)${DEV_TAG}
    - |
      echo "Installing foundation build dependencies"
      make python/install-dev

.gitlab_login:
  # sign into gitlab using a ssh token to allow for automated commits 
  script:
    - mkdir -p ~/.ssh
    - eval `ssh-agent`
    - echo "$GITLAB_RSA_PRIVATE_KEY" > ~/.ssh/gitlab_rsa
    - chmod 600 ~/.ssh/gitlab_rsa
    - ssh-add ~/.ssh/gitlab_rsa
    - ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
    - git remote set-url origin git@gitlab.com:${CI_PROJECT_PATH}.git
    - git config --global user.email "bob@construction-zone.gitlab.io"
    - git config --global user.name "the builder"
    - git config pull.ff only

lint:
  # runs a bunch of lint steps based on the foundation toggles
  extends:
    - .foundation
  stage: test
  only:
    - merge_requests
    - develop
    - main
  script:
    - > 
      if [[ "${FOUNDATION_CHECK_FORMAT}" == "true" ]]; then
        make python/check-format
      else
        echo skipping check FOUNDATION_CHECK_FORMAT is disabled with value \(${FOUNDATION_CHECK_FORMAT}\)
      fi
    - >
      if [[ "${FOUNDATION_CHECK_LINT}" == "true" ]]; then
        make python/check-lint
      else
        echo skipping check FOUNDATION_CHECK_LINT is disabled with value \(${FOUNDATION_CHECK_LINT}\)
      fi
    - >
      if [[ "${FOUNDATION_CHECK_IMPORTS}" == "true" ]]; then
        make python/check-sort-imports
      else
        echo skipping check FOUNDATION_CHECK_IMPORTS is disabled with value \(${FOUNDATION_CHECK_IMPORTS}\)
      fi
    - >
      if [[ "${FOUNDATION_CHECK_TYPES}" == "true" ]]; then
        make python/check-types;
      else
        echo skipping check FOUNDATION_CHECK_TYPES is disabled with value \(${FOUNDATION_CHECK_TYPES}\)
      fi
    - >
      if [[ "${FOUNDATION_CHECK_DOCS}" == "true" ]]; then
        make python/check-docs;
      else
        echo skipping check FOUNDATION_CHECK_DOCS is disabled with value \(${FOUNDATION_CHECK_DOCS}\)
      fi

test:
  extends:
    - .foundation
  stage: test
  only:
    - merge_requests
    - develop
    - main
  script:
    - make python/test

  coverage: /Total coverage:\s+([0-9.]+\%)/

package:
  # generate the python package and publish it to gitlab's package registry
  extends:
    - .foundation   
  stage: build
  only:
    variables:
      - ($CI_COMMIT_BRANCH == "main" || $CI_COMMIT_BRANCH == "develop") && $FOUNDATION_PUBLISH_PACKAGE == "true"
  except:
    - merge_requests
  script:
    - echo ${VERSION} > .bumpversion
    - make python/dist
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token make python/dist-upload

containerize:
  # generate docker images and publish them to gitlab's container registry
  stage: build
  only:
    variables:
      - ($CI_COMMIT_BRANCH == "main" || $CI_COMMIT_BRANCH == "develop") && $FOUNDATION_PUBLISH_IMAGES == "true"
  except:
    - merge_requests 
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:latest
  services:
    - name: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:dind
      alias: docker
  before_script:
     # login into registry.gitlab.com to be able to push the images
     # login into gitlab.com to leverage the packages dependency proxy, see
     # https://docs.gitlab.com/ee/user/packages/dependency_proxy/
    - |
      echo "login into registry.gitlab.com and gitlab.com"
      docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
      docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} gitlab.com
    # coreutils is needed for date --date to work on apline
    - |
      echo "Installing dev dependencies"
      apk add --update make coreutils git
      export DEV_TAG=.dev$(date +'%s' --date ${CI_PIPELINE_CREATED_AT})
      export VERSION=$(awk 'NR==2' .bumpversion)${DEV_TAG}
  script:
    - echo ${VERSION} > .bumpversion
    # clone the foundation artifacts
    - |
      export WORKING_DIR=/foundation/packages/python/
      cd /
      git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/construction-zone/bedrock/foundation.git --recurse-submodules   
      echo "Moving the cloned repository in scope of the foundation artifacts"
      cp -r ${CI_PROJECT_DIR} ${WORKING_DIR}
      cd ${WORKING_DIR}/${CI_PROJECT_NAME}
    - make docker/build-fast && make docker/build-debug-image
    # tag and upload the resulting images of make the commands
    - docker tag ${CI_PROJECT_NAME}:dev ${CI_REGISTRY_IMAGE}:${VERSION}
    - docker push ${CI_REGISTRY_IMAGE}:${VERSION}
    - docker tag ${CI_PROJECT_NAME}:debugpy ${CI_REGISTRY_IMAGE}:${VERSION}-debugpy
    - docker push ${CI_REGISTRY_IMAGE}:${VERSION}-debugpy

promote:
  extends:
    - .gitlab_login
  stage: release
  only:
    - develop
  script:
    - . venv/bin/activate; bump2version --allow-dirty patch
    - export VERSION=$(awk 'NR==2' .bumpversion)
    - export BRANCH_NAME=release/v${VERSION}-${CI_COMMIT_SHORT_SHA}
    - echo Creating release branch ${BRANCH_NAME}
    - git checkout -b ${BRANCH_NAME}
    - git fetch origin main
    - git merge origin/main -X ours --allow-unrelated-histories -m "sync develop with main"
    - git add .bumpversion .bumpversion.cfg
    - git commit --allow-empty -m "Release v${VERSION}"
    - >
      git push origin ${BRANCH_NAME}
      --push-option merge_request.create
      --push-option merge_request.target=main
      --push-option merge_request.title="${BRANCH_NAME}"
      --push-option merge_request.description="${BRANCH_NAME}"
  when: manual
  allow_failure: true

synchronize:
  # commits the version bump changes in main to develop
  extends:
    - .gitlab_login
  stage: release
  only:
    - main
  script:
    - export VERSION=$(awk 'NR==2' .bumpversion)
    - git fetch origin develop
    - git checkout develop
    - git pull
    - git merge origin/main -X theirs -m "Release v${VERSION}"
    - git push

pages:
  # generate static documentation & coverage html which will be deployed to gitlab pages
  extends:
    - .foundation
  stage: release
  script:
    - |
      make python/docs python/test
      mv docs/build/html ${CI_PROJECT_DIR}/public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $FOUNDATION_PUBLISH_PAGES == "true"
  allow_failure: true
